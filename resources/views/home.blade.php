@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    
                   <h1>Activities</h1>
                   <br>
                   <button type="button" onclick="actmodal()" class="btn btn-primary">New Activity</button>
                   <hr/>
                  
                    <ul id="actlist">
                        
                    </ul>

                        
                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.components')
<script>

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function cookiechk() {
    var myCookie = getCookie("user_token");
    console.log('check d2');
    console.log(myCookie);

    if (myCookie == null || myCookie == '') {

        alert('user token expired please login again');
        $('#logout-form').submit();
    }
    else {
        console.log('cookie exist');
    }
}



const actmodal = ()=> {

    const today = $('#today').val();
    $('#adate').val(today);
     $('#activity').val('');
     $('#act-id').val('');

    $('#activity-modal').modal('show');
}


const new_act = ()=>{

    let adate = $('#adate').val();
    let activity = $('#activity').val();
    let aid = $('#act-id').val();
    const uid = getCookie('user');

    const url = "/api/new-activity";
    const u_url = "/api/actvity/update";
     const data =   {
                    "aid":aid,
                    "activity":activity,
                    "adate": adate,
                    "user": uid
                    };


        if (aid == null || aid == '') {
            sendrequest(data,url);        
        } else {
            sendrequest(data,u_url);
        }
    
}

const upmodal = async(id)=>{
await cookiechk();
    const aid = id.value;
    var token = getCookie('user_token');
    const url = '/api/activity-info';
    const ud = {"aid": aid}

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(ud),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }
 const response = await fetch(url, settings);
 const data = await response.json();

     $('#adate').val(data.activity_date);
     $('#activity').val(data.activity);
     $('#act-id').val(data.id);
     load_data();

     $('#activity-modal').modal('show');
}



const remove = async(id)=>{
    await cookiechk();
    const aid = id.value;
    var token = getCookie('user_token');
    const url = '/api/activity/remove';
    const ud = {"aid": aid}

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(ud),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }


 Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then(async(result) => {
  if (result.value) {

    const response = await fetch(url, settings);
    const data = await response.json();
    if (data != null) {
        load_data();  
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your Activity Removed',
            showConfirmButton: false,
            timer: 1500
            });

    }
  }
})



}



const load_data = async()=>{
    await cookiechk();
    const uid = getCookie('user');
    var token = getCookie('user_token');
    const url = '/api/activity';
    const ud = {"user": uid}

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(ud),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }
 const response = await fetch(url, settings);
 const data = await response.json();
 console.log(data);

 $('#actlist').empty();
 data.forEach(res => {
        $('#actlist').append('<li><h3><a href=/details/'+res.id+'> | '+res.activity+'</a> '+
        '</h3><span>| <button type="button" onclick="upmodal(this)" value="'+res.id+'" class="btn-primary">Update</button> |'+
        ' <button type="button" onclick="remove(this)" value="'+res.id+'" class="btn-danger">Delete</button></span></li>');
 });

 

}





const sendrequest = async (data,url) => {
   
    var token = getCookie('user_token');

  const settings = {
    
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token,
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      
    }
  }

  const response = await fetch(url, settings);

  try {
    const data = await response.json();
    console.log(data.activity);

    const activity = $('#activity').val();

    if (activity == data.activity) {

        $('#activity').val('');
        $('#act-id').val('');

        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your Activity Saved',
            showConfirmButton: false,
            timer: 1500
            });
        load_data();
        $('#activity-modal').modal('hide');
    }

  } catch (err) {
    throw err;
  }
};


load_data();

</script>
@endsection
