<input type="hidden" id="today" value="{{$date}}">

<div class="modal" tabindex="-1" role="dialog" id="activity-modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Activity Details</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="act-id" />
                <label for="adate">Date</label>
                <input type="date" id="adate" class="form-control" value="{{$date}}" required>
                <label for="activity">Activity</label>
                <input type="text" class="form-control" id="activity" required/>
            </div>
            <div class="modal-footer">
              <button type="button" onclick="new_act()" class="btn btn-primary">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
</div>