
<input type="hidden" id="today" value="{{$date}}">
<input type="hidden" value="{{$aid}}" id="aid"/>
<div class="modal" tabindex="-1" role="dialog" id="note-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Note Taking</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="note-id" />
            <label for="ndate">Date</label>
            <input type="date" id="ndate" class="form-control" value="{{$date}}" required>
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" required/>
            <label for="body">Note</label>
            <textarea class = "form-control" id="body" cols="30" rows="10"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="new_note()" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="todo-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Todo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="todo-id" />
            <label for="tdate">Date</label>
            <input type="date" id="tdate" class="form-control" value="{{$date}}" required>
            <label for="todo">Todo Act</label>
            <input type="text" class="form-control" id="todo" required/>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="new_todo()" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>