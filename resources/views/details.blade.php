@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Denote App</div>
                    <div class="card-body">

                        
                    
                   

                   <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Notes </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Todos </a>
                        </li>
                      </ul>
                      <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <br/>
                            <h1>Notes List</h1>
                                <br>
                                <button type="button" onclick="notemodal()" class="btn btn-primary">New Note</button>
                                <hr/>
                                <div class="container nlist">
                                    {{-- Note Contents --}}
                                </div>

                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                                <h1>Todo List</h1>
                                <br>
                                <button type="button" onclick="todomodal()" class="btn btn-primary">Add Todo</button>
                                <hr/>
                               
                                 
                                <div class="container" id="todolist">
                                     
                                </div>
                        </div>
                       
                      </div>
                 
                   

                   </div>
                  

                        
            </div>
        </div>
    </div>



@endsection
@include('inc.dcomponent')

<script>

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function cookiechk() {
    var myCookie = getCookie("user_token");
    console.log('check d2');
    console.log(myCookie);

    if (myCookie == null || myCookie == '') {

        alert('user token expired please login again');
        $('#logout-form').submit();
    }
    else {
        console.log('cookie exist');
    }
}
const notemodal = ()=>{
    const today = $('#today').val();
    $('#note-id').val('');
    $('#ndate').val(today);
    $('#title').val('');
    $('#body').val('');

    $('#note-modal').modal('show');
}


const new_note = ()=>{

    let note = $('#note-id').val();
    let ndate = $('#ndate').val();
    let title = $('#title').val();
    let body = $('#body').val();
    let aid = $('#aid').val();
    const uid = getCookie('user');

    const nurl = "/api/notes/update";
    const url = "/api/new-note";

     const data =   {
                    "noteid":note,
                    "aid":aid,
                    "title":title,
                    "body": body,
                    "ndate": ndate,
                    "user": uid
                    };
        if (note == null || note == '') {
            sendrequest(data,url);
        } else {
            sendrequest(data,nurl);
        }


}

const upnote = async(id)=>{
    await cookiechk();
    const note_id = id.value;
    const url = '/api/note/details';
    var token = getCookie('user_token');

    var req = {
        "note_id": note_id
        }

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(req),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }
 const response = await fetch(url, settings);
 const data = await response.json();
 

console.log(data);

$('#note-id').val(data.id);
$('#ndate').val(data.note_date);
$('#title').val(data.title);
$('#body').val(data.body);

 
$('#note-modal').modal('show');

}





const load_data = async()=>{
    await cookiechk();
    const uid = getCookie('user');
    var token = getCookie('user_token');
    var aid = getCookie('aid');

    const url = '/api/view-notes';
    const urlt = '/api/view-todo';
    

     console.log(aid);
    var ud = {
        "user": uid,
         "aid":aid
        }

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(ud),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }
 const response = await fetch(url, settings);
 const todoresponse = await fetch(urlt, settings);

 const data = await response.json();
 const todos = await todoresponse.json();


 console.log(data);
 console.log(todos);

 $('.nlist').empty();
 $('#todolist').empty();

    data.forEach(note => {

        $('.nlist').append(' <div class="card-header"><div class="row"><div class="col-md-9">'+note.title+' </div> '+
        '<div class="col-md-3"><button class="btn-primary" value="'+note.id+'" onclick="upnote(this)">Details</button> | '+
        '<button class="btn-danger" value="'+note.id+'" onclick="rmvnote(this)">Delete</button></div></div></div> '+
        '<div class="card-body">'+note.body+'</div>');
        
    });

    todos.forEach(todo => {
        // <div class="card-header">Denote App</div>

        if (todo.status == '0') {
            $('#todolist').append('<div class="card-header"><div class="row"><div class="col-md-8">'+
        '<input type="checkbox" class="form-check-input" value="'+todo.id+'" onclick="chktodo(this)"/>'+
        '<label class="form-check-label" for="exampleCheck1" id="chklabel-'+todo.id+'">'+todo.todo+'</label> </div>'+
        '<div class="col-md-4"><button class="btn-primary" value="'+todo.id+'" onclick="tdnote(this)">EDIt</button> | '+
        '<button class="btn-danger" value="'+todo.id+'" onclick="rmvtodo(this)">Delete</button></div> '+
        '</div>');
            
        } else {
            $('#todolist').append('<div class="card-header"><div class="row"><div class="col-md-8">'+
        '<input type="checkbox" class="form-check-input" value="'+todo.id+'" onclick="chktodo(this)" checked />'+
        '<label class="form-check-label wordline" for="exampleCheck1" id="chklabel-'+todo.id+'">'+todo.todo+'</label> </div>'+
        '<div class="col-md-4"><button class="btn-primary" value="'+todo.id+'" onclick="tdnote(this)">EDIt</button> | '+
        '<button class="btn-danger" value="'+todo.id+'" onclick="rmvtodo(this)">Delete</button></div> '+
        '</div>');
            
        }
       
    });

}



const rmvnote = async(id)=>{
    await cookiechk();
    const nid = id.value;
    
    var token = getCookie('user_token');
    const url = '/api/notes/remove';
    const note = {"noteid": nid};

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(note),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }

 Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then(async(result) => {
  if (result.value) {
    
    const response = await fetch(url, settings);
    const data = await response.json();

        if (data != null) {

            Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your Note Removed',
            showConfirmButton: false,
            timer: 1500
            });

            load_data();    
        }

  }
})


}

const rmvtodo = async(id)=>{
    await cookiechk();
    const tid = id.value;
    
    var token = getCookie('user_token');
    const url = '/api/todo/remove';
    const note = {"todo_id": tid};

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(note),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }

 Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then(async(result) => {
  if (result.value) {
    const response = await fetch(url, settings);
     const data = await response.json();

        if (data != null) {
            Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your Todo Removed',
            showConfirmButton: false,
            timer: 1500
            });

            load_data();    
        }
    }

  })
}




const sendrequest = async (data,url) => {
    await cookiechk();
   var token = getCookie('user_token');

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(data),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token,
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
     
   }
 }

 const response = await fetch(url, settings);
 console.log(response);
 try {
   const data = await response.json();
    console.log(data);
   // const title = $('#title').val();
        $('#title').val('');
        $('#body').val('');
        load_data();
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your Note Saved',
            showConfirmButton: false,
            timer: 1500
            });
        $('#note-modal').modal('hide');
        
 } catch (err) {
   throw err;
 }
};

const todomodal = ()=>{

    $('#todo-modal').modal('show');

}

const tdnote = async(id)=>{

    await cookiechk();
    const todo = id.value;
    var token = getCookie('user_token');
    const url = '/api/todo/info';
    const req = {"tdid": todo};

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(req),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token
   }
 }
 const response = await fetch(url, settings);
 const data = await response.json();

    $('#todo-id').val(data.id);
    $('#tdate').val(data.todo_date);
    $('#todo').val(data.todo);

     load_data();

    $('#todo-modal').modal('show');
}


const new_todo = ()=>{

    let todoid = $('#todo-id').val();
    let tdate = $('#tdate').val();
    let todo = $('#todo').val();
    let aid = $('#aid').val();
    const uid = getCookie('user');

            const url = "/api/new-todo";
            const nurl = "/api/todo/update";

     const data =   {
                    "todo_id":todoid,
                    "aid":aid,
                    "todo":todo,
                    "tdate": tdate,
                    "user": uid
                    };


if (todoid == null || todoid == '') {
    todorequest(data,url);
}else{
    console.log('update');
    console.log(data);

    todorequest(data,nurl);

}
      
}



const todorequest = async (data,url) => {
    await cookiechk();
   var token = getCookie('user_token');

 const settings = {
   
   method: 'POST',
   body: JSON.stringify(data),
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
     'Authorization': 'Bearer '+token,
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
     
   }
 }

 const response = await fetch(url, settings);

 try {
   const data = await response.json();

        const todo = $('#todo').val();
        console.log(data.todo);

        $('#todo').val('');
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your Todo Saved',
            showConfirmButton: false,
            timer: 1500
            });

        $('#todo-modal').modal('hide');
        load_data();
        
    

 } catch (err) {
   throw err;
 }
};




load_data();
</script>

<script>
    const chktodo = async(id)=>{
        await cookiechk();
       const todoid = id.value;
        var token = getCookie('user_token');
        //$('#chklabel-'+todoid).addClass('wordline');
        const url = '/api/todo/status';
        const data = {'todo_id':todoid};
                const settings = {
        
                    method: 'POST',
                    body: JSON.stringify(data),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+token,
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        
                    }
            }
    
 const response = await fetch(url, settings);

try {
  const data = await response.json();

  Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your Todo Updated',
            showConfirmButton: false,
            timer: 1500
    });

    if (data.status == '1') {
        $('#chklabel-'+data.id).addClass('wordline');
    } else {
        $('#chklabel-'+data.id).removeClass('wordline');
    }

 
       
   

} catch (err) {
  throw err;
}

        
    }

</script>