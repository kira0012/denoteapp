<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Activity extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'activity_date' => $this->activity_date,
            'activity' => $this->activity,
            'notes' => $this->notes,
            'todos' => $this->todos,
        ];
    }
}
