<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        setcookie("user_token", $accessToken, time()+3600);
        setcookie("user",auth()->user()->id, time()+3600);


        $date = date('Y-m-d');
        return view('home')
        ->with('date',$date);
    }

    public function activity_details($aid){


        setcookie("aid", $aid, time()+3600);

        $date = date('Y-m-d');
        return view('details')
        ->with('aid',$aid)
        ->with('date',$date);

    }

   
}
