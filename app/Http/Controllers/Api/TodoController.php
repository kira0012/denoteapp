<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Todo;

class TodoController extends Controller
{
    //

    public function get_todo(Request $request){


        $notes = Todo::where('activity_id',$request->input('aid'))
        ->orderby('id','desc')
        ->get();

        return response()->json($notes);

    }

    public function todo_info(Request $request){

        $todo = Todo::findorfail($request->input('tdid'));

        return response()->json($todo);
    }

    public function todo_store(Request $request){

        //return $request->all();

        $todo = New Todo;

        $todo->todo_date = $request->input('tdate');
        $todo->activity_id = $request->input('aid');
        $todo->todo = $request->input('todo');
        $todo->status = 0;
        $todo->save();
        
        return response()->json($todo);
    }

    public function todo_update(Request $request){


        $todo = Todo::findorfail($request->input('todo_id'));

        $todo->todo_date = $request->input('tdate');    
        $todo->todo = $request->input('todo');
        $todo->status = 0;
        $todo->save();
        
        return response()->json($todo);

    }

    public function todo_remove(Request $request){

        $todo = Todo::findorfail($request->input('todo_id'));

        $status = 'done';

        $todo->delete();

        return response()->json($status);
    }

    public function todo_status(Request $request){

        $todo = Todo::findorfail($request->input('todo_id'));

        if ($todo->status == '0') {
            $todo->status = 1;
            $todo->save();
        } else {
            # code...
            $todo->status = 0;
            $todo->save();
        }

        return response()->json($todo);
        

    }

}
