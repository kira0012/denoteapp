<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Activity;
use App\User;
use App\Http\Resources\User as UserOrganizer;
use Auth;

class ActivityController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function user_organizer(Request $request){


        $user = $request->input('id');
        $data = UserOrganizer::collection(User::find($user));

        return $data;

    }

    public function index(Request $request){

        $activity = Activity::where('user_id',$request->input('user'))->orderby('id','desc')->get();

        return response()->json($activity);
    }

    public function activity_details(Request $request){

            $activity = Activity::findorfail($request->input('aid'));

            return response()->json($activity);
    }

    public function activity_store(Request $request){
       

        $request->validate([
            'adate' => 'required',
            'activity' => 'required'
        ]);

        $activity = New Activity;
        $activity->activity_date = $request->input('adate');
        $activity->activity = $request->input('activity');
        $activity->user_id = $request->input('user');
        $activity->save();

        return response()->json($activity);

    }

    public function update_activity(Request $request){

        $request->validate([
            'adate' => 'required',
            'activity' => 'required'
        ]);

        $activity = Activity::findorfail($request->input('aid'));
        $activity->activity_date = $request->input('adate');
        $activity->activity = $request->input('activity');
        $activity->user_id = $request->input('user');
        $activity->save();

        return response()->json($activity);

    }

    public function remove_activity(Request $request){

        $aid = $request->input('aid');

        $activity = Activity::findorfail($aid);
        $activity->delete();

        $status = "done";

        return response()->json($status);
    }
    
}
