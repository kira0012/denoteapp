<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Note;
// use App\Model\Activity;

class NoteController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function get_notes(Request $request){

        //return $request->all();

        $notes = Note::where('activity_id',$request->input('aid'))
        ->orderby('id','desc')
        ->get();

        return response()->json($notes);

    }

    public function single_note(Request $request){

        $notes = Note::findorfail($request->input('note_id'));

        return response()->json($notes);


    }

    public function note_store(Request $request){

        $note = New Note;
        
        $note->activity_id = $request->input('aid');
        $note->title = $request->input('title');
        $note->body = $request->input('body');
        $note->note_date = $request->input('ndate');
        $note->save();

        return response()->json($note);

    }

    public function update_note(Request $request){


        $note = Note::findorfail($request->input('noteid'));         
        $note->activity_id = $request->input('aid');
        $note->title = $request->input('title');
        $note->body = $request->input('body');
        $note->note_date = $request->input('ndate');
        $note->save();

        return response()->json($note);


    }

    public function remove_note(Request $request){

        $note = Note::findorfail($request->input('noteid'));

        $note->delete();

        $status = 'done';

        return response()->json($status);
    }

}
