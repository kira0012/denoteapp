<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    

    public function login(Request $request){

        $user_data = $request->validate([
           
            'email' => 'required|email',
            'password' => 'required|'
        ]);

        if (!auth()->attempt($user_data)) {
            # code...
            return response(['message' => 'Invalid Username Or Password']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken]);

    }
}
