<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //

    protected $guard = [];

    public function activity(){

        return $this->belongsTo(Activity::class,'activity_id');
    }
}
