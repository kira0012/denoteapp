<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //

    protected $guard = [];

    public function user(){

        return $this->belongsTo('App\User','user_id');
    }

    public function notes(){

        return $this->hasMany(Note::class,'activity_id');
    }

    public function todos(){

        return $this->hasMany(Todo::class,'activity_id');
    }
}
