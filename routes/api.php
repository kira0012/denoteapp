<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login','Api\AuthController@login');

//activity
Route::post('/user/organizer','Api\ActivityController@user_organizer');

Route::post('/activity','Api\ActivityController@index');
Route::post('/new-activity','Api\ActivityController@activity_store');
Route::post('/activity-info','Api\ActivityController@activity_details');
Route::post('/actvity/update','Api\ActivityController@update_activity');
Route::post('/activity/remove','Api\ActivityController@remove_activity');


Route::post('/view-notes','Api\NoteController@get_notes');
Route::post('/note/details','Api\NoteController@single_note');
Route::post('/new-note','Api\NoteController@note_store');
Route::post('/notes/update','Api\NoteController@update_note');
Route::post('/notes/remove','Api\NoteController@remove_note');


Route::post('/view-todo','Api\TodoController@get_todo');
Route::post('/new-todo','Api\TodoController@todo_store');
Route::post('/todo/info','Api\TodoController@todo_info');
Route::post('/todo/update','Api\TodoController@todo_update');
Route::post('/todo/remove','Api\TodoController@todo_remove');
Route::post('/todo/status','Api\TodoController@todo_status');
